from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_print, diplomacy_eval



class TestDiplomacy (TestCase):


	def test_read_1(self):
		s = "A Madrid Hold\nB London Support A\n"
		i = diplomacy_read(s)
		self.assertEqual(i, [["A","Madrid","Hold"],["B","London","Support","A"]])


	def test_read_2(self):
		s = "A Madrid Hold\nC London Support B\nD Austin Move London\n"
		i = diplomacy_read(s)
		self.assertEqual(i, [["A","Madrid","Hold"],["C","London","Support","B"],["D","Austin","Move","London"]])

	def test_read_3(self):
		s = "A Madrid Hold\n"
		i = diplomacy_read(s)
		self.assertEqual(i, [["A","Madrid","Hold"]])

	def test_solve_1(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
		w = StringIO()
		diplomacy_solve(r,w)
		self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

	def test_solve_2(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
		w = StringIO()
		diplomacy_solve(r,w)
		self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

	def test_solve_3(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n D Austin Move London\n")
		w = StringIO()
		diplomacy_solve(r,w)
		self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


	def test_print_1(self):
		w = StringIO()
		diplomacy_print([["A", "Madrid"]], w)
		self.assertEqual(w.getvalue(), "A Madrid\n")

	def test_print_2(self):
		w = StringIO()
		diplomacy_print([["E", "[dead]"]], w)
		self.assertEqual(w.getvalue(), "E [dead]\n")

	def test_print_3(self):
		w = StringIO()
		diplomacy_print([["B", "Austin"]], w)
		self.assertEqual(w.getvalue(), "B Austin\n")
	
	def test_eval_1(self):
		i = [["A","Madrid","Hold"]]
		o = [["A","Madrid"]]
		v = diplomacy_eval(i)
		self.assertEqual(o,v)

	def test_eval_2(self):
		i = [["A","Madrid","Hold"],["B","London","Move","Madrid"]]
		o = [["A","[dead]"],["B","[dead]"]]
		v = diplomacy_eval(i)
		self.assertEqual(o,v)

	def test_eval_3(self):
		i = [["A","Madrid","Hold"],["B","Barcelona","Move","Madrid"],["C","London","Support","B"],["D","Austin","Move","London"]]
		o = [["A","[dead]"],["B","[dead]"],["C","[dead]"],["D","[dead]"]]
		v = diplomacy_eval(i)
		self.assertEqual(o,v)


if __name__ == "__main__":
    main()
